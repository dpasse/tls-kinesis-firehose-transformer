#!groovy​
import static java.util.UUID.randomUUID

//noinspection GroovyAssignabilityCheck
properties([
    buildDiscarder(logRotator(numToKeepStr: '5'))
])

try {
    def templateName = 'template'
    def projectName = 'tls-kinesis-firehose-transformer'
    def naRegion = 'us-east-1'
    def emeaRegion = 'eu-central-1'
    def anzRegion = 'ap-southeast-2'
    def uuid = randomUUID() as String

    withEnv(["NODE_ENV='test'", "SLS_IGNORE_WARNING='*'", "SLS_DEBUG='*'"]) {
        stage('CI Build & Deploy') {

            node('linux') {
                step([$class: 'WsCleanup'])
                checkout([
                    $class           : 'GitSCM',
                    branches         : [[name: "*/master"]],
                    userRemoteConfigs: [
                        [credentialsId: 'MkeDevelopmentKey', url: "git@bitbucket.org:mkeonekey/tls-kinesis-firehose-transformer.git"]
                    ],
                    extensions       : [
                        [$class: 'CleanBeforeCheckout'],
                        [$class: 'LocalBranch', localBranch: 'master'],
                        [$class: 'UserExclusion', excludedUsers: 'MkeDevelopment'],
                        [$class: 'MessageExclusion', excludedMessage: '.*\\[ci-ignore\\].*'],
                    ]
                ])

                def version = updateVersion()

                sh 'npm install'

                sh 'npm test'
                sh 'npm run cover'

                junit 'reports/test_results.xml'
                def scannerHome = tool 'Sonar'
                withSonarQubeEnv {
                    sh "\"${scannerHome}\\bin\\sonar-scanner\" -Dproject.settings=sonar-project.properties -Dsonar.projectVersion=$version"
                }

                try {
                    def stage = 'dev'
                    sh "serverless deploy -s ${stage} -l -r ${naRegion} -v"
                    // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                    sh "serverless deploy -s ${stage} -l -r ${emeaRegion} -v"
                    // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                    sh "serverless deploy -s ${stage} -l -r ${anzRegion} -v"
                    // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                    // sh "aws lambda add-permission --function-name arn:aws:lambda:${naRegion}:853609764768:function:${projectName}${stage} --source-arn arn:aws:execute-api:${naRegion}:853609764768:weh6f0g8k0/*/PUT/events --principal apigateway.amazonaws.com --statement-id ${uuid} --action lambda:InvokeFunction --region ${naRegion}"
                    // sh "endpoint=${naEndpoint}${stage} key=${keys[stage]} npm run integration"

                    junit 'reports/int_results.xml'
                } catch (Exception e) {
                    throw e
                }
            }
        }

        stage('Test Deploy Gate') {
            input 'Deploy to Test?'
            node('linux') {
                def stage = 'test'
                sh "serverless deploy -s ${stage} -l -r ${naRegion} -v"
                // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                sh "serverless deploy -s ${stage} -l -r ${emeaRegion} -v"
                // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                sh "serverless deploy -s ${stage} -l -r ${anzRegion} -v"
                // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                // sh "aws lambda add-permission --function-name arn:aws:lambda:${naRegion}:853609764768:function:${projectName}${stage} --source-arn arn:aws:execute-api:${naRegion}:853609764768:weh6f0g8k0/*/PUT/events --principal apigateway.amazonaws.com --statement-id ${uuid} --action lambda:InvokeFunction --region ${naRegion}"
                // sh "endpoint=${naEndpoint}${stage} key=${keys[stage]} npm run integration"
            }
        }

        stage('Stage Deploy Gate') {
            input 'Deploy to Stage?'
            node('linux') {
                def stage = 'staging'
                sh "serverless deploy -s ${stage} -l -r ${naRegion} -v"
                // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                sh "serverless deploy -s ${stage} -l -r ${emeaRegion} -v"
                // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                sh "serverless deploy -s ${stage} -l -r ${anzRegion} -v"
                // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                // sh "aws lambda add-permission --function-name arn:aws:lambda:${naRegion}:853609764768:function:${projectName}${stage} --source-arn arn:aws:execute-api:${naRegion}:853609764768:weh6f0g8k0/*/PUT/events --principal apigateway.amazonaws.com --statement-id ${uuid} --action lambda:InvokeFunction --region ${naRegion}"
                // sh "endpoint=${naEndpoint}${stage} key=${keys[stage]} npm run integration"
            }
        }

        stage('Prod Deploy Gate') {
            input 'Deploy to Production?'
            withEnv(["NODE_ENV='production'"]) {
                node('linux') {
                    def stage = 'prod'
                    sh "serverless deploy -s ${stage} -l -r ${naRegion} -v"
                    // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                    sh "serverless deploy -s ${stage} -l -r ${emeaRegion} -v"
                    // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                    sh "serverless deploy -s ${stage} -l -r ${anzRegion} -v"
                    // sh "serverless invoke -f ${templateName} -s ${stage} -l -p invoke-data.json -v"

                    // sh "aws lambda add-permission --function-name arn:aws:lambda:${naRegion}:853609764768:function:${projectName}${stage} --source-arn arn:aws:execute-api:${naRegion}:853609764768:weh6f0g8k0/*/PUT/events --principal apigateway.amazonaws.com --statement-id ${uuid} --action lambda:InvokeFunction --region ${naRegion}"
                    // sh "endpoint=${naEndpoint}${stage} key=${keys[stage]} npm run integration"
                }
            }
        }
    }
}
finally {

}

def updateVersion() {
    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'GIT_LOGIN', usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
        try {
            sh "npm version patch"
        } catch (all) {
            echo all.message
        }

        sh "git push \"https://${GIT_USERNAME}:${GIT_PASSWORD.replace('%', '%%')}@bitbucket.org/mkeonekey/tls-kinesis-firehose-transformer.git\" --tags"
        sh "git push \"https://${GIT_USERNAME}:${GIT_PASSWORD.replace('%', '%%')}@bitbucket.org/mkeonekey/tls-kinesis-firehose-transformer.git\""
    }
    def parsed = readJSON file: "package.json"
    return parsed.version
}
