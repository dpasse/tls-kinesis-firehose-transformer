const expect = require('chai').expect;

const cleanRecord = require('../src/handler').cleanRecord;
const convertBase64ToAsciiString = require('../src/handler').convertBase64ToAsciiString;
const convertAsciiStringToBase64 = require('../src/handler').convertAsciiStringToBase64;

const handle = require('../src/handler').handle;

describe('cleanRecord', ()=> {
  it('will remove user data from request when headers are present.', () => {
    process.env.PurgeUserData = 'true';
    
    const data = '{ "stageVariables": { }, "requestContext": { "identity": { "userAgent": "blah", "sourceIp": "123.123.123.123" } }, "headers": { "Authorization": "blahblah", "X-Forwarded-For":"123.123.123.123", "User-Agent": "Test" } }';
    let result = cleanRecord(data);

    let usable = JSON.parse(result);
    expect(usable.headers).to.exist;
    expect(usable.headers['X-Forwarded-For']).to.equal(null);
    expect(usable.headers.Authorization).to.equal(null);
    expect(usable.stageVariables).to.equal(null);
    expect(usable.headers['User-Agent']).to.equal(null);
    expect(usable.requestContext.identity['userAgent']).to.equal(null);
    expect(usable.requestContext.identity['sourceIp']).to.equal(null);
  });

  it('will add cleaned attributes if missing from headers.', () => {
    process.env.PurgeUserData = 'true';
    
    const data = '{ "stageVariables": {}, "headers": { } }';
    let result = cleanRecord(data);

    let usable = JSON.parse(result);
    expect(usable.headers).to.exist;
    expect(usable.headers.Authorization).to.equal(null);
    expect(usable.stageVariables).to.equal(null);
    expect(usable.headers['User-Agent']).to.equal(null);
  });

  it('will not scrub user data when setting is turned off.', () => {
    process.env.PurgeUserData = 'false';
    
    const data = '{ "stageVariables": { }, "requestContext": { "identity": { "userAgent": "blah", "sourceIp": "123.123.123.123" } }, "headers": { "Authorization": "blahblah", "X-Forwarded-For":"123.123.123.123", "User-Agent": "Test" } }';
    let result = cleanRecord(data);

    let usable = JSON.parse(result);
    expect(usable.headers).to.exist;
    expect(usable.headers['X-Forwarded-For']).to.equal("123.123.123.123");
    expect(usable.headers.Authorization).to.equal(null);
    expect(usable.stageVariables).to.equal(null);
    expect(usable.headers['User-Agent']).to.equal("Test");
    expect(usable.requestContext.identity['userAgent']).to.equal("blah");
    expect(usable.requestContext.identity['sourceIp']).to.equal("123.123.123.123");
  });
});

describe('convertBase64ToAsciiString', ()=> {
  it('will convert Base64 to Ascii.', () => {
    const data = 'eyAic291cmNlSXAiOiAiMTIzLjEyMy4xMjMuMTIzIiwgInRlc3QiOiAidGVzdCIgfQ==';
    let result = convertBase64ToAsciiString(data);

    expect(result).to.exist;
    expect(result).to.equal('{ "sourceIp": "123.123.123.123", "test": "test" }');
  });
});

describe('convertAsciiStringToBase64', ()=> {
  it('will convert Ascii to Base64.', () => {
    const data = '{ "sourceIp": "123.123.123.123", "test": "test" }';
    let result = convertAsciiStringToBase64(data);

    expect(result).to.exist;
    expect(result).to.equal('eyAic291cmNlSXAiOiAiMTIzLjEyMy4xMjMuMTIzIiwgInRlc3QiOiAidGVzdCIgfQ==');
  });
});

describe('handle', () => {
  it('will handle and not purge user data.', () => {
    let event = {
        "records": [ 
            {
                "data": "eyAiaGVhZGVycyI6IHsgInNvdXJjZUlwIjogIjEyMy4xMjMuMTIzLjEyMyIsICJVc2VyLUFnZW50IjogInRlc3QiIH0sICJib2R5IjogeyAiZGV0YWlscyI6IFsgeyAidGVzdCI6InRlc3QiIH1dIH0gfQ=="
            }
        ]
    };

    let callback = null;
    process.env.PurgeUserData = 'false';
    handle(event, null, (_, p2) => { callback = p2; });
    
    results = callback.records;
    expect(results[0].result).to.equal('Ok');
    expect(results[0].data).to.equal('eyJoZWFkZXJzIjp7InNvdXJjZUlwIjoiMTIzLjEyMy4xMjMuMTIzIiwiVXNlci1BZ2VudCI6InRlc3QiLCJBdXRob3JpemF0aW9uIjpudWxsfSwiYm9keSI6eyJkZXRhaWxzIjpbeyJ0ZXN0IjoidGVzdCJ9XX19Cg==');
  });

  it('will handle and purge user data.', () => {
    let event = {
        "records": [ 
            {
                "data": "eyAiaGVhZGVycyI6IHsgInNvdXJjZUlwIjogIjEyMy4xMjMuMTIzLjEyMyIsICJVc2VyLUFnZW50IjogInRlc3QiIH0sICJib2R5IjogeyAiZGV0YWlscyI6IFsgeyAidGVzdCI6InRlc3QiIH1dIH0gfQ=="
            }
        ]
    };

    let callback = null;
    process.env.PurgeUserData = 'true';
    handle(event, null, (_, p2) => { callback = p2; });
    
    results = callback.records;
    expect(results[0].result).to.equal('Ok');
    expect(results[0].data).to.equal('eyJoZWFkZXJzIjp7InNvdXJjZUlwIjoiMTIzLjEyMy4xMjMuMTIzIiwiVXNlci1BZ2VudCI6bnVsbCwiQXV0aG9yaXphdGlvbiI6bnVsbCwiWC1Gb3J3YXJkZWQtRm9yIjpudWxsLCJ4LWNsaWVudC1pZCI6bnVsbH0sImJvZHkiOnsiZGV0YWlscyI6W3sidGVzdCI6InRlc3QifV19fQo=');
  });
});