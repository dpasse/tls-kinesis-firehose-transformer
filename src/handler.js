'use strict';

const convertBase64ToAsciiString = exports.convertBase64ToAsciiString = (data) => {
    const payload = Buffer.from(data, 'base64').toString('ascii');
    return payload.replace(/\\"/g, '"');
};

const convertAsciiStringToBase64 = exports.convertAsciiStringToBase64 = (data) => {
    return Buffer.from(data).toString('base64');
};

const cleanRecord = exports.cleanRecord = (data) => {
    let parsed = JSON.parse(data);

    if (parsed.stageVariables) {
        parsed.stageVariables = null;
    }

    if (parsed.headers) {
        parsed.headers['Authorization'] = null;
        if (process.env.PurgeUserData == 'true') {
            if (parsed.headers['Forwarded']) {
                parsed.headers['Forwarded'] = null;
            }

            parsed.headers['X-Forwarded-For'] = null;
            parsed.headers['User-Agent'] = null;
            parsed.headers['x-client-id'] = null;

            if (parsed.requestContext && parsed.requestContext.identity) {
                parsed.requestContext.identity['userAgent'] = null;
                parsed.requestContext.identity['sourceIp'] = null;
            }
        }
    }

    if (parsed['multiValueHeaders']) {
        parsed['multiValueHeaders'] = null;
    }

    // put it back into string form.
    return data = JSON.stringify(parsed);
};

exports.handle = (event, context, callback) => {
    const output = event.records.map((record) => {
        // Base64 to String.
        let result = convertBase64ToAsciiString(record.data);

        // clean record, if applicable.
        result = cleanRecord(result) + '\n';

        // Put record back to Base64.
        result = convertAsciiStringToBase64(result);

        return {
            recordId: record.recordId,
            result: 'Ok',
            data: result
        };
    });

    callback(null, { records: output });
};